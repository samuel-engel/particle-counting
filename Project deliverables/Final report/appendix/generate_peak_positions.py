import numpy as np
def generate_peak_positions(min_gap_in_ns=12, max_gap_in_ns=70):
	peak_positions_collection = []
	total_measurements = 0
	for gap in range(min_gap_in_ns, max_gap_in_ns + 1):
		peak_positions = np.arange(0, 100, gap)
		for peak_count in range(1, len(peak_positions)+1):
			total_measurements += 1
			filename = "peak-" + str(peak_count) + " gap-" + str(gap)
			peak_positions_collection.append((list(peak_positions[:peak_count]), filename))
			
	print("Total measurements:", total_measurements)
	seconds_per_measurement = 30
	print("Estimated time:", round(((total_measurements * seconds_per_measurement)/3600), 2), "hours")

	return peak_positions_collection

peak_positions_collection = generate_peak_positions()
