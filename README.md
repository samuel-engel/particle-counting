# Application of machine learning towards particle counting and identification
The project contains 3 self-contained algorithms for particle counting using various ML approaches.
## Overview of algorithms
### PPCA
The peak property based counting algorithm (PPCA) works on the principle of peaks having certain mathematical properties which define them. The PPCA incorporates peak height, width, prominence, distance between peaks and an optional threshold. Instead of setting the parameters manually, the algorithm uses labelled data to find the optimal attribute ranges such that all peaks are counted correctly. Establishing the peak properties of the labelled peaks allows the algorithm to learn the mathematical property ranges which contain all the labelled peaks.
### STMF
Shape-based template matching framework (STMF) is based on the peak shape as a whole, instead of looking at the individual peak properties. This approach mimics
the way humans identify particle peaks and subsequently count particles. A template is constructed for each peak group representing a group of 1, 2, 3 or more particles. All test segments are compared to every template to determine the most similar template.
### CNN
The one-dimensional convolutional neural network (CNN) uses discriminative supervised learning to count particles. It learns features from raw time series data rather than through engineered features such as the peak shape or properties used for the previous two algorithms. The input data is segmented into even-sized windows before the model is trained. The model learns the intrinsic features which it then uses to classify a given window into labels of the number of peaks it contains.


## How to install and run
### Requirements
The project uses version 3 of Python. Make sure [requirements in requirements.txt are satisfied](requirements.txt)
### Setup and running
1. Select one or more algorithms and put them in your working directory along with the [library of shared functions](Algorithms/shared.py).
2. Set up directories to put time series data (voltage over time) in `.trc`, `.csv` or `.p` format. 
   * Create a *train* and *test* subdirectories if using the training mode. 
   * Create a *classify* subdirectory if using the classifying mode.
3. Run the chosen algorithm with the `--help` flag to learn about it's parameters


