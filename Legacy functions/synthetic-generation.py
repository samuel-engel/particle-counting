import pickle
import random
import numpy as np

def generate_random_gaps():
	peak_positions_collection = []
	while len(peak_positions_collection) < 70000:
		peak_positions = [0]
		while peak_positions[-1] <= 88:
			gap = random.randint(12, 70)
			next_peak_position = peak_positions[-1] + gap
			if next_peak_position <= 100:
				peak_positions.append(next_peak_position)
		if peak_positions not in peak_positions_collection and len(peak_positions) > 3:
			peak_positions_collection.append(peak_positions)
			print(peak_positions)
			print(len(peak_positions_collection))

	pickle.dump(peak_positions_collection, open("synthetic2.p", "wb"))

def generate_peak_positions(min_gap_in_ns=12, max_gap_in_ns=70):
	peak_positions_collection = []
	peak_positions_dict = {}
	total_measurements = 0
	for gap in range(min_gap_in_ns, max_gap_in_ns + 1):
		peak_positions = np.arange(0, 100, gap)
		for peak_count in range(1, len(peak_positions)+1):
			total_measurements += 1
			# print(peak_positions[:peak_count])
			filename = "peak-" + str(peak_count) + " gap-" + str(gap)
			# print(filename)
			peak_positions_collection.append((list(peak_positions[:peak_count]), filename))
			peak_positions_dict[filename] = list(peak_positions[:peak_count])

	print("Total measurements:", total_measurements)
	seconds_per_measurement = 30
	print("Estimated time:", round(((total_measurements * seconds_per_measurement)/3600), 2), "hours")
	peak_count = 0
	for peak_positions in peak_positions_collection:
		peak_count += len(peak_positions)
	print("Number of peaks:", peak_count)
	# pickle.dump(peak_positions_dict, open("Raw data/synthetic-codes-new.p", "wb"))

	return peak_positions_collection
def decode():
	codes = ["peak-1 gap-12", "peak-2 gap-12", "peak-3 gap-12", "peak-4 gap-12", "peak-5 gap-12", "peak-6 gap-12", "peak-7 gap-12", "peak-8 gap-12", "peak-9 gap-12"]
	peak_positions_dict = pickle.load(open("Raw data/synthetic-codes.p", "rb"))
	print(peak_positions_dict)
	for code in codes:
		print(peak_positions_dict[code])

def load():
	peak_positions = pickle.load(open("synthetic.p", "rb"))
	print(len(peak_positions))


# capture()
# load()
generate_peak_positions()
# peakPositionNs = np.arange(0, 100, 12)
# decode()
# print("Goal:", peakPositionNs)
