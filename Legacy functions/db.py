import sqlite3
from sqlite3 import Error


def create_connection(db_file="../Data storage/time-series.db"):
	connection = None
	try:
		connection = sqlite3.connect(db_file)
		enforce_foreign_key_constraint(connection)
	except Error as e:
		print(e)
	return connection


def enforce_foreign_key_constraint(conn):
	cur = conn.cursor()
	cur.execute("PRAGMA foreign_keys = ON;")
	conn.commit()


def insert_test_experiment(conn):
	sql = ''' INSERT INTO experiment(filename,instrument_name) VALUES(?,?) '''
	cur = conn.cursor()
	cur.execute(sql, ("test filename", "test instrument"))
	conn.commit()
	return cur.lastrowid


def insert_test_data(conn):
	sql = ''' insert into data (experimentID, "time", voltage) values (?, ?, ?); '''
	cur = conn.cursor()
	cur.execute(sql, (1, 318962478.595901, -12.4143423158693))
	conn.commit()
	return cur.lastrowid


def insert_timeseries_data(conn, timeseries):
	sql = ''' insert into data (experimentID, "time", voltage) values (?, ?, ?); '''
	cur = conn.cursor()
	# cur.execute(sql, (1, 318962478.595901, -12.4143423158693))
	cur.executemany('INSERT INTO data VALUES(null, ?,?,?);', timeseries)
	conn.commit()
	return cur.rowcount


def insert_experiment(conn, metadata):
	sql = ''' INSERT INTO experiment VALUES(null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) '''
	cur = conn.cursor()
	cur.execute(sql, metadata)
	conn.commit()
	return cur.lastrowid


def get_voltage(conn, time, experiment_id):
	sql = ''' SELECT * FROM data WHERE time = ? AND experimentID = ?'''
	cur = conn.cursor()
	cur.execute(sql, (time, experiment_id))
	conn.commit()
	return cur.fetchall()


def find_ID(conn, time, experiment_id):
	sql = ''' SELECT ID FROM data WHERE time = ?  AND experimentID = ?'''
	cur = conn.cursor()
	cur.execute(sql, (time, experiment_id))
	conn.commit()
	return cur.fetchone()


def get_segment_above_threshold(conn, ID_range, threshold, experiment_id):
	sql = ''' SELECT time, voltage FROM data where ID between ? and ? AND voltage < ? AND experimentID = ?'''
	cur = conn.cursor()
	cur.execute(sql, ID_range + (threshold, experiment_id))
	conn.commit()
	if ID_range[1] < ID_range[0]: print("Inverted range -> empty result set")
	return cur.fetchall()


def get_segment_above_threshold_indices(conn, ID_range, threshold, experiment_id):
	sql = ''' SELECT ID FROM data where ID between ? and ? AND voltage < ? AND experimentID = ?'''
	cur = conn.cursor()
	cur.execute(sql, ID_range + (threshold, experiment_id))
	conn.commit()
	if ID_range[1] < ID_range[0]: print("Inverted range -> empty result set")
	return cur.fetchall()


def get_timeseries_size(conn):
	sql = ''' SELECT ID FROM data ORDER BY ID DESC LIMIT 1'''
	cur = conn.cursor()
	cur.execute(sql)
	conn.commit()
	return cur.fetchone()


def find_rows_in_range(conn, ID_range, experiment_id):
	sql = ''' SELECT time, voltage FROM data where ID between ? and ? AND experimentID = ?'''
	cur = conn.cursor()
	cur.execute(sql, ID_range + (experiment_id,))
	conn.commit()
	if ID_range[1] < ID_range[0]: print("Inverted range -> empty result set")
	return cur.fetchall()

