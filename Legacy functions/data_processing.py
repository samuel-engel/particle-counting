import db
import readTrc

import pickle
import matplotlib.pyplot as plt
import numpy as np
import time


def bootstrapping():
	i = 6
	# for i in range(28):
	# 	print("File", i)
	filename = "C2sci124Xe-p39000" + str(4).zfill(2)
	print("File:", filename)
	start = time.time()
	timeseries_A = pickle.load(open("Raw data/" + filename + ".p", "rb"))
	timeseries_A[:, 1] = timeseries_A[:, 1] * -1

	end = time.time()
	# print(end - start)

	filename2 = "C2sci124Xe-p39000" + str(i + 1).zfill(2)
	print("File:", filename2)
	timeseries_B = pickle.load(open("Raw data/" + filename2 + ".p", "rb"))
	timeseries_B[:, 1] = timeseries_B[:, 1] * -1

	# newTimeseries = (abs(timeseries[:, 1]) + abs(timeseries2[:, 1]))
	# timeseries3 = np.stack((timeseries[:, 0], newTimeseries), axis=1)

	# data_visualization.preview_timeseries_section(timeseries3)
	preview_new_timeseries(timeseries_A, timeseries_B, 1100000, 2200000)
	del timeseries_A
	del timeseries_B
	# print("Raw data/" + filename + ".p")
	# pickle.dump(timeseries, open("Raw data/" + filename[:-4] + ".p", "wb"))
	# print("Saved:", "Raw data/" + filename[:-4] + ".p")
	print("hehy")


def preview_new_timeseries(timeseries_A, timeseries_B, range_l, range_u):
	figure, axis = plt.subplots(3, sharex=True)
	# data_visualization.preview_timeseries_section(timeseries_A[range_l:range_u])
	# data_visualization.preview_timeseries_section(timeseries_B[range_l:range_u])
	timeseries_result = (timeseries_A[:, 1] + timeseries_B[:, 1])
	timeseries_result /= 2
	fontsize = 15
	axis[2].set_xlabel("Time in ns", fontsize=fontsize)

	axis[0].plot(timeseries_A[:, 1][range_l:range_u], color="#00799C")
	axis[0].set_title("Time series A", fontsize=fontsize)
	# plt.show()
	axis[1].plot(timeseries_B[:, 1][range_l:range_u], color="#FAAF00")
	axis[1].set_title("Time series B", fontsize=fontsize)
	axis[2].plot(timeseries_result[range_l:range_u], color="#00B938")
	axis[2].set_title("Resulting time series", fontsize=fontsize)
	figure.tight_layout()

	figure.add_subplot(111, frameon=False)
	# hide tick and tick label of the big axis
	plt.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
	plt.ylabel("Voltage in V", fontsize=fontsize)

	def on_resize(event):
		figure.tight_layout()
		figure.canvas.draw()

	cid = figure.canvas.mpl_connect('resize_event', on_resize)
	plt.show()
	# fig = plt.figure()
	# plt.plot(timeseries_result, figure=fig)
	# plt.show()
	plt.close(figure)


def np2csv(np_array, filename):
	np.savetxt(filename + ".csv", np_array, delimiter=",")


def trc2csv(input_file):
	x, y, metaData = readTrc.readTrc(input_file)
	x_and_y = np.stack((x * 1.e9, y * 1000.), axis=1)
	# print(x_and_y[:10])
	np.savetxt("test.csv", x_and_y, delimiter=",")


def convert_to_working_units(time_in_seconds, voltage_in_volts):
	time_in_ns = time_in_seconds * 1.e9
	voltage_in_mV = voltage_in_volts * 1000.
	return time_in_ns, voltage_in_mV


def load_trc_to_database(conn, filepath):
	start = time.time()
	x, y, metadata = readTrc.readTrc("./" + filepath)
	end = time.time()
	print("Converting .trc to numpy array took: " + str(end - start) + " co")
	metadata_export = list(metadata.values())
	filename = filepath.split("/")[-1]
	metadata_export.insert(0, filename)
	experiment_ID = db.insert_experiment(conn, metadata_export)
	experiment_ID_column = np.full(x.shape, experiment_ID)
	timeseries = np.stack((experiment_ID_column, x, y), axis=1)
	total_rows_inserted = 0
	record_count_per_insertion = 2000000
	start = time.time()
	for sub_array in np.array_split(timeseries, int(x.size / record_count_per_insertion)):
		sub_timeseries = list(map(tuple, sub_array))
		total_rows_inserted += db.insert_timeseries_data(conn, sub_timeseries)
		print("Inserted: " + str(int((total_rows_inserted / x.size) * 100)) + "%")
	end = time.time()
	print("Loading numpy array into database took: " + str(end - start))
	return total_rows_inserted


def serialize_raw_data():
	for i in range(28):
		print("File", i)
		filename = "C2sci124Xe-p39000" + str(i).zfill(2) + ".trc"
		print("File:", filename)
		x, voltage, metaData = readTrc.readTrc("Raw data/" + filename)
		timeseries = np.stack((x * 1.e9, voltage * -1), axis=1)
		# print("Raw data/" + filename + ".p")
		pickle.dump(timeseries, open("Raw data/" + filename[:-4] + ".p", "wb"))
		print("Saved:", "Raw data/" + filename[:-4] + ".p")


def prepare_samples():
	timeseries = pickle.load(open("./Raw data/C2sci124Xe-p3900005.p", "rb"))
	plt.plot(timeseries[:, 1][1815000:])
	plt.show()
	# with open("Working directory/classify/sample.p", "wb") as file:
	# 		pickle.dump(timeseries, file)
	print("")


if __name__ == '__main__':
	# bootstraping()
	prepare_samples()

# conn = db.create_connection()
# print(conn)
# trc2csv("Raw data/C2sci124Xe-p3900001.trc")
# print("Total rows inserted into database: " + str(load_trc_to_database(conn, "Raw data/C2sci124Xe-p3900001.trc")))
