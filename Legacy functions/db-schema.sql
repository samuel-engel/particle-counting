DROP TABLE IF EXISTS experiment;
DROP TABLE IF EXISTS data;

CREATE TABLE IF NOT EXISTS experiment (
  [ID] INTEGER PRIMARY KEY,
  [filename] TEXT,
  [instrument_name] TEXT,
  [instrument_number] INTEGER,
  [trace_label] TEXT,
  [wave_array_count] INTEGER,
  [pnts_per_screen] INTEGER,
  [first_valid_pnt] INTEGER,
  [last_valid_pnt] INTEGER,
  [first_point] INTEGER,
  [sparsing_factor] INTEGER,
  [segment_index] INTEGER,
  [subarray_count] INTEGER,
  [sweeps_per_acq] INTEGER,
  [points_per_pair] INTEGER,
  [pair_offset] INTEGER,
  [vertical_gain] INTEGER,
  [vertical_offset] INTEGER,
  [max_value] INTEGER,
  [min_value] INTEGER,
  [nominal_bits] INTEGER,
  [nom_subarray_count] INTEGER,
  [horiz_interval] INTEGER,
  [horiz_offset] INTEGER,
  [pixel_offset] INTEGER,
  [vertunit] TEXT,
  [horunit] TEXT,
  [horiz_uncertainty] INTEGER,
  [trigger_time] TEXT,
  [acq_duration] INTEGER,
  [record_type] TEXT,
  [processing_done] TEXT,
  [ris_sweeps] INTEGER,
  [timebase] TEXT,
  [vert_coupling] TEXT,
  [probe_att] INTEGER,
  [fixed_vert_gain] TEXT,
  [bandwidth_limit] TEXT,
  [vertical_vernier] INTEGER,
  [acq_vert_offset] INTEGER,
  [wave_source] INTEGER,
  [user_text] TEXT
);

CREATE TABLE IF NOT EXISTS data (
  [ID] INTEGER PRIMARY KEY,
  [experimentID] INTEGER,
  [time] INTEGER,
  [voltage] INTEGER,
  CONSTRAINT fk_experiment FOREIGN KEY ([experimentID]) REFERENCES [experiment] ([ID])
);

