dtaidistance==2.3.6
matplotlib==3.5.2
more-itertools==8.13.0
numpy==1.22.3
scipy==1.8.0
tensorflow==2.8.0

