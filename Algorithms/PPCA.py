import argparse
import csv
import json
import pickle
import sys
from statistics import mean
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
import time
import shared
import os
import shared


class PPCA:
	default_parameters = {
		"height-bounds": [0.14, None],
		"prominence-bounds": [0.0002, 0.7336],
		"width-bounds": [None, None],
		"distance": None,
		"threshold-bounds": None
	}
	points_per_batch = 50000

	def __init__(self, arguments):
		self.arguments = arguments
		self.parameters = {
			"height-bounds": [None, None],
			"prominence-bounds": [None, None],
			"width-bounds": [None, None],
			"distance": None,
			"threshold-bounds": [None, None]
		}
		keys = [key.replace("-bounds", "") + "s" for key in self.parameters.keys()]
		self.peak_properties = dict.fromkeys(keys, [])
	def classify2(self, hasLabels=False):
		path = self.arguments.dir + "classify/"
		shared.verify_directory_exists(path)
		self.load_weights()
		classification_results = []
		test_results = []
		if hasLabels:
			valid_timeseries = shared.get_collection_of_valid_timeseries(path, True, self.arguments.flip_y)
		else:
			valid_timeseries = shared.get_collection_of_valid_timeseries(path, False, self.arguments.flip_y)
		# self.__plot_all_timeseries(valid_timeseries)
		# wrong_label = []
		for timeseries_data in valid_timeseries:
			# timeseries_data.timeseries = timeseries_data.timeseries[:1000000]
			print("Classifying", timeseries_data.filename)
			start = time.time()
			voltages = timeseries_data.timeseries[:, 1]
			hist, bins = np.histogram(voltages)
			print(bins)
			self.parameters["height-bounds"] = [bins[2], None]
			if self.arguments.verbose:
				print("Weights set:", self.parameters)
			self.save_weights()

			peak_coordinates = self.classify_timeseries(timeseries_data.timeseries)
			end = time.time()
			# if len(peak_coordinates) != int(timeseries_data.filename[5:6]):
			# 	wrong_label.append(timeseries_data.filename)
			print("> Number of peaks in file", timeseries_data.filename + ":", len(peak_coordinates))
			if self.arguments.verbose: print("> Processing took:", round(end - start, 4), "seconds")
			# self.__plot_all_posible_peaks(timeseries_data)
			classification_results.append((timeseries_data.filename, len(peak_coordinates)))

			if hasLabels:
				peak_centre_times = peak_coordinates[:, 0]
				test_result = list(self.evaluate_classification_results(timeseries_data, peak_centre_times))
				test_result.insert(0, timeseries_data.filename)
				test_results.append(test_result)
			# total_loss += test_result[2] + test_result[3]
			# accuracies.append(test_result[4])



			if self.arguments.graphs:
				if self.arguments.no_interact:
					print_graph = True
				else:
					print_graph = shared.query_yes_no("> Would you like to plot the classified peaks?")
				if print_graph:
					print("> Plotting graph")
					plt.plot(timeseries_data.timeseries[:, 0], timeseries_data.timeseries[:, 1], label="Input timeseries")
					plt.plot(peak_coordinates[:, 0], peak_coordinates[:, 1], "x", label="Detected peaks")
					plt.title(timeseries_data.filename)
					plt.legend(loc="upper right")
					plt.show()

			if self.arguments.save: self.save_classification_results(classification_results)
			if self.arguments.save and hasLabels:
				self.save_training_results(test_results)
			# print("Incorrectly detected", wrong_label)

	def classify(self):
		path = self.arguments.dir + "classify/"
		shared.verify_directory_exists(path)
		self.load_weights()
		results = []
		valid_timeseries = shared.get_collection_of_valid_timeseries(path, False, self.arguments.flip_y)
		# self.__plot_all_timeseries(valid_timeseries)
		for timeseries_input in valid_timeseries:
			# timeseries_input.timeseries = timeseries_input.timeseries[:1000000]
			print("Classifying", timeseries_input.filename)
			start = time.time()
			peak_coordinates = self.classify_timeseries(timeseries_input.timeseries)
			end = time.time()
			print("> Number of peaks in file", timeseries_input.filename + ":", len(peak_coordinates))
			if self.arguments.verbose: print("> Processing took:", round(end - start, 4), "seconds")
			# self.__plot_all_posible_peaks(timeseries_input)
			results.append((timeseries_input.filename, len(peak_coordinates)))
			if self.arguments.graphs:
				if self.arguments.no_interact:
					print_graph = True
				else:
					print_graph = shared.query_yes_no("> Would you like to plot the classified peaks?")
				if print_graph:
					print("> Plotting graph")
					plt.plot(timeseries_input.timeseries[:, 0], timeseries_input.timeseries[:, 1], label="Input timeseries")
					plt.plot(peak_coordinates[:, 0], peak_coordinates[:, 1], "x", label="Detected peaks")
					plt.title(timeseries_input.filename)
					plt.legend(loc="upper right")
					plt.show()


		if self.arguments.save: self.save_classification_results(results)
	def __plot_all_posible_peaks(self, timeseries_input):
		peaks, props = signal.find_peaks(timeseries_input.timeseries[:, 1], height=[None, None], threshold=[None, None], distance=None, prominence=[None, None], width=[None, None])
		peak_coordinates = timeseries_input.timeseries[peaks]
		props_to_keep = [19, 36]
		print(props_to_keep)
		for prop, values in props.items():
			new_values = []
			for index in props_to_keep:
				new_values.append(values[index])
			props[prop] = new_values
		print(props)
		print(self.parameters)
		plt.plot(timeseries_input.timeseries[:, 0], timeseries_input.timeseries[:, 1], label="Input timeseries")
		plt.plot(peak_coordinates[:, 0], peak_coordinates[:, 1], "x", label="Detected peaks")
		for i, coordinate in enumerate(peak_coordinates):
			plt.text(coordinate[0], coordinate[1], f'{i}', fontsize=12)

		plt.legend(loc="upper right")
		plt.title("All possible peaks")
		plt.show()

	def __plot_all_timeseries(self, timeseries_inputs):
		for timeseries_input in timeseries_inputs:
			plt.plot(timeseries_input.timeseries[:, 0], timeseries_input.timeseries[:, 1], label=timeseries_input.filename)
			plt.legend(loc="upper right")
			plt.title("All peaks")
		plt.show()
	def classify_timeseries(self, timeseries):
		peak_coordinates = None
		sub_timeseries_count = int(len(timeseries) / self.points_per_batch)
		if sub_timeseries_count == 0:
			sub_timeseries_count = 1
		for i, sub_timeseries in enumerate(np.array_split(timeseries, sub_timeseries_count)):
			peaks, _ = signal.find_peaks(sub_timeseries[:, 1], height=self.parameters["height-bounds"], prominence=self.parameters["prominence-bounds"], distance=self.parameters["distance"], threshold=self.parameters["threshold-bounds"], width=self.parameters["width-bounds"])
			if self.arguments.verbose and i % 100 == 0: print(str(round(((i/sub_timeseries_count)*100), 1)) + "% done")
			if peak_coordinates is not None:
				peak_coordinates = np.concatenate((peak_coordinates, sub_timeseries[peaks]), axis=0)
			else:
				peak_coordinates = sub_timeseries[peaks]
		return peak_coordinates

	def load_weights(self):
		weights_filepath = self.arguments.dir + 'PPCA/weights.json'
		if os.path.exists(weights_filepath):
			with open(weights_filepath) as infile:
				parameters = json.load(infile)
				self.parameters = parameters
				print("Weights loaded:", parameters)
		else:
			print("Weights file was not found. Expected location:" + weights_filepath)
			sys.exit()

	def save_classification_results(self, results):
		output = [["Filename", "Peak count", "Parameter", "Parameter value"]]
		for parameter in self.parameters.items():
			output.append(["", "", parameter[0], parameter[1]])
		for i, result in enumerate(results):
			if i < len(self.parameters):
				output[i + 1][0] = result[0]
				output[i + 1][1] = result[1]
			else:
				output.append([result[0], result[1], "", ""])
		filename = "Classification results " + time.strftime("%Y-%m-%d %H:%M:%S")
		filepath = self.arguments.dir + "PPCA/" + filename + ".csv"
		with open(filepath, 'w') as f:
			write = csv.writer(f)
			write.writerows(output)
		print("Successfully saved results in:", filepath)

	def train(self):
		train_path = self.arguments.dir + "train/"
		shared.verify_directory_exists(train_path)
		start = time.time()
		for timeseries_data in shared.get_collection_of_valid_timeseries(train_path, True):
			print(f"Collecting peak properties for {timeseries_data.filename}")
			self.collect_peak_properties(timeseries_data)
		# self.set_initial_weights()

		self.optimize()
		end = time.time()
		self.save_weights()
		if self.arguments.verbose:
			print("Weights found:", self.parameters)
			print("Training took:", round(end-start, 2), "seconds")
		self.test()
	def train2(self, folder_name):
		train_path = self.arguments.dir + folder_name + "/"
		shared.verify_directory_exists(train_path)
		start = time.time()
		height_min_wegihts = []
		for timeseries_data in shared.get_collection_of_valid_timeseries(train_path, False):
			print(f"Training based on {timeseries_data.filename}")
			voltages = timeseries_data.timeseries[:, 1]
			hist, bins = np.histogram(voltages)
			print(bins)
			height_min_wegihts.append(bins[2])

		self.parameters["height-bounds"] = [min(height_min_wegihts), None]
		self.save_weights()
		self.test()

		end = time.time()
		if self.arguments.verbose:
			print("Weights found:", self.parameters)
			print("Training took:", round(end-start, 2), "seconds")
	def test(self):
		test_path = self.arguments.dir + "test/"
		results = []
		total_loss = 0
		accuracies = []
		for timeseries_data in shared.get_collection_of_valid_timeseries(test_path, True):
			peak_coordinates = self.classify_timeseries(timeseries_data.timeseries)
			peak_centre_times = peak_coordinates[:, 0]
			result = list(self.evaluate_classification_results(timeseries_data, peak_centre_times))
			result.insert(0, timeseries_data.filename)
			results.append(result)
			total_loss += result[2] + result[3]
			accuracies.append(result[4])

		if self.arguments.save:
			self.save_training_results(results)
		return total_loss, mean(accuracies)

	def evaluate_classification_results(self, timeseries_data, test_labels):
		false_negatives = np.setdiff1d(timeseries_data.labels, test_labels)
		false_positives = np.setdiff1d(test_labels, timeseries_data.labels)
		peaks_in_both = np.intersect1d(timeseries_data.labels, test_labels)
		# todo: should consider false positives as well as false negatives
		# accuracy = (len(peaks_in_both) / len(timeseries_data.labels) * 100)
		accuracy = ((len(peaks_in_both))/ (len(peaks_in_both) + len(false_positives) + len(false_negatives)))*100
		if self.arguments.graphs and not self.arguments.no_interact:
			if shared.query_yes_no("Would you like to see a graph of the classification for file" + str(timeseries_data.filename) + "?"):
				false_negative_indices = np.where(np.isin(timeseries_data.timeseries[:, 0], false_negatives))[0]
				for peak_index in false_negative_indices:
					plt.figure()
					peak_l = peak_index-200
					peak_u = peak_index+200
					peak_timeseries = timeseries_data.timeseries[peak_l:peak_u]
					plt.vlines(timeseries_data.labels, color="red")
					plt.plot(peak_timeseries[:, 0], peak_timeseries[:, 1])
					peaks, props = signal.find_peaks(peak_timeseries[:, 1], threshold=(None, None), prominence=(None, None), height=(0.1, None), width=(None, None), distance=None)
					plt.plot(peak_timeseries[:, 0][peaks], peak_timeseries[:, 1][peaks], "x")
					# maybe = self.classify_timeseries(peak_timeseries)
					plt.show()
					# plt.show(block=False)
					print(peaks)
					print(props)
				plt.show()
		# if True:
		# 	fig = plt.figure()
		# 	voltage = timeseries[:, 1]
		# 	time = timeseries[:, 0]
		# 	plt.vlines(false_negatives, max(voltage)+0.01, 0, colors="#FA2200", alpha=0.5, label="Only in GSI algorithm")
		# 	plt.vlines(false_positives, max(voltage)+0.01, 0, colors="#000F4B", alpha=0.5, label="Only in PPCA")
		# 	plt.vlines(peaks_in_both, max(voltage), 0, colors="#00B938", alpha=0.5, label="In both algorithms")
		# 	plt.plot(time, voltage, figure=fig, color="#00799C")
		# 	plt.ylabel("Voltage in V", fontsize=fontsize)
		# 	plt.xlabel("Time in ns", fontsize=fontsize)
		# 	plt.legend(loc="upper left")
		# 	plt.show()
		return len(peaks_in_both), len(false_negatives), len(false_positives), accuracy

	def collect_peak_properties(self, timeseries_data):
		peak_indices = np.where(np.isin(timeseries_data.timeseries[:, 0], timeseries_data.labels))[0]
		timeseries_voltage = timeseries_data.timeseries[:, 1]
		if self.arguments.verbose: print("> Collecting prominence information")
		prominences = signal.peak_prominences(timeseries_voltage, peak_indices)
		if self.arguments.verbose: print("> Collecting width information")
		widths = signal.peak_widths(timeseries_voltage, peak_indices, prominence_data=prominences)
		# with open("./prom-width.p", "wb") as file:
		# 	pickle.dump((prominences, widths), file)
		# with open("./prom-width.p", "rb") as file:
		# 	prom_width = pickle.load(file)
		# prominences = prom_width[0]
		# widths = prom_width[1]
		if self.arguments.verbose: print("> Collecting height information")
		heights = timeseries_voltage[peak_indices]
		# distances =
		self.peak_properties["prominences"] = self.peak_properties["prominences"] + list(prominences[0])
		self.peak_properties["widths"] = self.peak_properties["widths"] + list(widths[0])
		self.peak_properties["heights"] = self.peak_properties["heights"] + list(heights)
		if self.arguments.graphs:
			sample_size = 50
			peak_indices_sample = peak_indices[:sample_size]
			contour_heights = timeseries_voltage[peak_indices_sample] - prominences[0][:sample_size]
			plt.plot(timeseries_voltage[:3500000])
			plt.plot(peak_indices_sample, timeseries_voltage[peak_indices_sample], "x", label="Peak")
			plt.hlines(widths[1][:sample_size], widths[2][:sample_size], widths[3][:sample_size], color="#FAAF00", label="Width")
			plt.hlines(widths[1][:sample_size-1], widths[3][:sample_size-1], widths[2][1:sample_size], color="#0021AA", label="Distance")
			plt.vlines(x=peak_indices_sample, ymin=contour_heights, ymax=timeseries_voltage[peak_indices_sample], color="#FA2200", alpha=0.8, label="Prominence")
			plt.vlines(x=peak_indices_sample, ymin=0, ymax=timeseries_voltage[peak_indices_sample], color="#00B938", linestyles="dotted", label="Height", linewidth=2)
			plt.legend(loc="upper right")

			plt.show()

	def optimize(self):

		optimal_weights = self.default_parameters
		optimal_loss = 1000000
		for weight in ["height", "prominence", "width"]:
			print("Optimizing", weight, "bounds")

			weight_min = min(self.peak_properties[weight + "s"])
			self.parameters[weight + "-bounds"] = [weight_min, None]
			loss, accuracy = self.test()
			print("> Optimized lower bound with loss:", loss, "and accuracy:", accuracy)
			if loss < optimal_loss:
				optimal_weights = dict(self.parameters)
				optimal_loss = loss

			weight_max = max(self.peak_properties[weight + "s"])
			self.parameters[weight + "-bounds"] = [weight_min, weight_max]
			loss, accuracy = self.test()
			if loss < optimal_loss:
				optimal_weights = dict(self.parameters)
				optimal_loss = loss
			print("> Optimized upper and lower bounds with loss:", loss, "and accuracy:", accuracy)
		print("Optimal weights:", optimal_weights, "with a loss of", optimal_loss)
		self.parameters = optimal_weights

	def save_training_results(self, results):
		output = [["Filename", "Correctly detected peaks", "False negative (not detected)", "False positive (detected extra)", "Accuracy (%)", "Parameter", "Parameter value"]]
		for parameter in self.parameters.items():
			output.append(["", "", "", "", "", parameter[0], parameter[1]])
		accuracies = []
		for i, result in enumerate(results):
			accuracies.append(result[4])
			if i < len(self.parameters):
				output[i + 1][0] = result[0]
				output[i + 1][1] = result[1]
				output[i + 1][2] = result[2]
				output[i + 1][3] = result[3]
				output[i + 1][4] = result[4]
			else:
				output.append([result[0], result[1], result[2], result[3], result[4]])
		filename = "Training results " + time.strftime("%Y-%m-%d %H:%M:%S")
		filepath = self.arguments.dir + "PPCA/" + filename + ".csv"
		print("Total accuracy is:", str(mean(accuracies)) + "%")
		with open(filepath, 'w') as f:
			write = csv.writer(f)
			write.writerows(output)
		print("Successfully saved results in:", filepath)

	def save_weights(self):
		with open(self.arguments.dir + 'PPCA/weights.json', 'w') as outfile:
			json.dump(self.parameters, outfile, indent=4, sort_keys=True)


if __name__ == '__main__':
	CLI = shared.generate_arg_parser(
		"The Peak Property-based Counting Algorithm is used to train and classify timeseries data using an algorithm based on mathematical peak properties")
	args = CLI.parse_args()
	model = PPCA(args)
	if args.mode == "classify":
		model.classify2(True)
	elif args.mode == "train":
		model.train()
	elif args.mode == "test":
		model.load_weights()
		model.test()
