import csv
import datetime
import os
import pickle
import random
import time
import numpy as np
import shared
import tensorflow as tf
import matplotlib.pyplot as plt


class CNN:
	default_parameters = {
		"window-size": 200,
		"prune": True,  # prune redundant baseline windows
		"prune-factor": 10,
		"filters-count": 8,
		"kernel-size": 2,
		"pool-size": 2,
		"dropout-rate": 0.5
	}

	def __init__(self, arguments, parameters=None):
		self.arguments = arguments
		self.model = None
		if parameters is None:
			self.parameters = CNN.default_parameters
		else:
			self.parameters = parameters
		self.train_filenames = []
		self.test_filenames = []
		self.train_labels = None
		self.train_windows = None
		self.test_labels = None
		self.test_windows = None
		self.output = [[None for _ in range(10)] for _ in range(28)]
		self.output[0] = ["Run number", "Scores", "Time took(s)", "Accuracy", "+/-", "Train filenames",
						  "Test filenames", "Parameter", "Parameter value"]


	def segment_into_windows(self, timeseries_data, type_of_data):
		timeseries = timeseries_data.timeseries
		number_of_windows = int(len(timeseries) / self.parameters["window-size"])
		# trim the end of the timeseries to get even windows
		trim_by = len(timeseries) % number_of_windows
		if trim_by != 0:
			timeseries = timeseries[:-trim_by]
		windows = np.split(timeseries, number_of_windows)
		if self.arguments.graphs :
			print("")
		if type_of_data == "train" or type_of_data == "test":
			self.add_labels_to_windows(windows, timeseries_data.labels, type_of_data)
		elif type_of_data == "classify":
			return np.asarray(windows)

	@staticmethod
	def save_pickled_file(variable, filepath):
		with open(filepath, "wb") as file:
			pickle.dump(variable, file)

	def add_labels_to_windows(self, windows, timeseries_labels, type_of_data):
		start = time.time()
		window_labels = []

		baseline_only_count = 0
		baseline_only_kept_count = 0
		processed_windows = []
		for window_index, window in enumerate(windows):
			peaks_in_window = len(np.intersect1d(timeseries_labels, window[:, 0]))

			window_max_voltage = max(window[:, 1])
			if type_of_data == "train" and self.parameters["prune"] and window_max_voltage < 0.03 and peaks_in_window == 0:
				baseline_only_count += 1

				if window_index % self.parameters["prune-factor"] == 0:
					processed_windows.append(window)
					window_labels.append(peaks_in_window)
					baseline_only_kept_count += 1
			else:
				window_labels.append(peaks_in_window)
				processed_windows.append(window)

			if self.arguments.verbose:
				print("Window: " + str(window_index) + "/" + str(len(windows)))

		if type_of_data == "train" and self.arguments.verbose:
			print("Windows with only baseline:", baseline_only_count)
			print("Kept", baseline_only_kept_count, "baseline-only windows")
		# if self.arguments.graphs:
		# 	fig = plt.figure()
		# 	for window in processed_windows[:1000]:
		# 		plt.plot(window[:, 0], window[:, 1])
		# 		plt.ylabel("Voltage in V")
		# 		plt.xlabel("Time in ns")
		# 	plt.show()
		window_labels = np.array(window_labels)
		self.save_pickled_file(window_labels, "window_labels.p")
		if self.arguments.graphs:
			hist, bins, _ = plt.hist(window_labels, range(max(window_labels)+2))
			print(hist)
			# print(bins)
			plt.title("Histogram")
			xticks = bins[:-1]+0.5
			plt.xticks(xticks, list(map(str, bins[:-1])))
			for index, value in enumerate(hist):
				if value > 0:
					plt.text(xticks[index], value/2, int(value), ha='center')

			plt.show()
		windows = np.array(processed_windows)
		window_labels = np.asarray(np.split(window_labels, len(window_labels)))
		windows = np.asarray(windows)
		end = time.time()

		if self.arguments.verbose: print("Labeling took", round(end - start, 2), "seconds")
		if type_of_data == "train":
			if self.train_labels is None and self.train_windows is None:
				self.train_windows = windows
				self.train_labels = window_labels
			else:
				self.train_windows = np.concatenate((self.train_windows, windows), axis=0)
				self.train_labels = np.concatenate((self.train_labels, window_labels), axis=0)
		elif type_of_data == "test":
			if self.test_labels is None and self.test_windows is None:
				self.test_windows = windows
				self.test_labels = window_labels
			else:
				self.test_windows = np.concatenate((self.test_windows, windows), axis=0)
				self.test_labels = np.concatenate((self.test_labels, window_labels), axis=0)
	def segment_train_test(self):
		# reset for when running multiple train calls on the same object
		self.train_labels = None
		self.train_windows = None
		self.test_labels = None
		self.test_windows = None

		train_path = self.arguments.dir + "train/"
		shared.verify_directory_exists(train_path)
		for timeseries_data in shared.get_collection_of_valid_timeseries(train_path, True):
			self.segment_into_windows(timeseries_data, "train")
			self.train_filenames.append(timeseries_data.filename)
		test_path = self.arguments.dir + "test/"
		for timeseries_data in shared.get_collection_of_valid_timeseries(test_path, True):
			self.segment_into_windows(timeseries_data, "test")
			self.test_filenames.append(timeseries_data.filename)


		print("Number of train segments saved", len(self.train_windows))
		print("Number of test segments saved", len(self.test_windows))

		self.hot_encode_class_integers()

		self.save_pickled_file(((self.train_windows, self.train_labels), (self.test_windows, self.test_labels)),
							   self.arguments.dir + "CNN/train_test_windows.p")

	def hot_encode_class_integers(self):
		# Specify the max unique number of labels across both label sets
		max_label = np.max(np.hstack((self.train_labels.flatten(), self.test_labels.flatten())))
		num_classes = int(max_label) + 1
		self.train_labels = tf.keras.utils.to_categorical(self.train_labels, num_classes=num_classes)
		self.test_labels = tf.keras.utils.to_categorical(self.test_labels, num_classes=num_classes)

	def load_train_test_windows(self):
		train_test_windows_filepath = self.arguments.dir + "CNN/train_test_windows.p"
		train_test_windows = shared.extract_from_file(train_test_windows_filepath)
		if train_test_windows is not None:
			self.train_windows = train_test_windows[0][0]
			self.train_labels = train_test_windows[0][1]
			self.test_windows = train_test_windows[1][0]
			self.test_labels = train_test_windows[1][1]
			print("Number of train segments loaded:", len(self.train_windows))
			print("Number of test segments loaded:", len(self.test_labels))
			print("Loaded train and test windows data successfully")
			return True
		else:
			print('Warning: Segmented data not found. Loading from', train_test_windows_filepath)
			return False

	def evaluate(self, trainX, trainy, testX, testy):
		verbose, epochs, batch_size = 0, 10, 32
		log_dir = self.arguments.dir + "CNN/logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
		tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
		n_timesteps, n_features, n_outputs = trainX.shape[1], trainX.shape[2], trainy.shape[1]
		model = tf.keras.models.Sequential()
		model.add(
			tf.keras.layers.Conv1D(filters=self.parameters["filters-count"], kernel_size=self.parameters["kernel-size"],
								   activation='relu', input_shape=(n_timesteps, n_features)))
		model.add(
			tf.keras.layers.Conv1D(filters=self.parameters["filters-count"], kernel_size=self.parameters["kernel-size"],
								   activation='relu'))
		model.add(tf.keras.layers.Dropout(self.parameters["dropout-rate"]))
		model.add(tf.keras.layers.MaxPooling1D(pool_size=self.parameters["pool-size"]))
		model.add(tf.keras.layers.Flatten())
		model.add(tf.keras.layers.Dense(100, activation='relu'))
		model.add(tf.keras.layers.Dense(n_outputs, activation='softmax'))
		model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
		# fit network
		model.fit(trainX, trainy, epochs=epochs, batch_size=batch_size, verbose=verbose)
		# model.fit(trainX, trainy, epochs=epochs, batch_size=batch_size, verbose=verbose, callbacks=[tensorboard_callback])
		# evaluate model
		_, accuracy = model.evaluate(testX, testy, batch_size=batch_size, verbose=0)
		self.model = model
		return accuracy

	# summarize scores
	def summarize_results(self, scores):
		m, s = np.mean(scores), np.std(scores)
		print(scores)
		print('Accuracy: %.3f%% (+/-%.3f)' % (m, s))

	def save_results(self, scores, durations):
		self.model.save(self.arguments.dir + "CNN/model")
		m, s = np.mean(scores), np.std(scores)
		for i in range(len(scores)):
			self.output[i + 1][0] = i + 1
			self.output[i + 1][1] = scores[i]
			self.output[i + 1][2] = durations[i]
		self.output[1][3] = ('%.3f' % m)
		self.output[1][4] = ('%.3f' % s)
		for i in range(len(self.train_filenames)):
			self.output[i + 1][5] = self.train_filenames[i][0]
		for i in range(len(self.test_filenames)):
			self.output[i + 1][6] = self.test_filenames[i][0]

		for i, parameter in enumerate(self.parameters):
			self.output[i + 1][7] = parameter
			self.output[i + 1][8] = self.parameters[parameter]
		output_filename = "Classification results " + time.strftime("%Y-%m-%d %H:%M:%S")
		filepath = self.arguments.dir + "CNN/" + output_filename + ".csv"

		with open(filepath, 'w') as f:
			write = csv.writer(f)
			write.writerows(self.output)

	def train(self, repeats=1):
		# Load train and test windows
		self.segment_train_test()
		loaded_successfully = self.load_train_test_windows()
		if not loaded_successfully:
			self.segment_train_test()

		scores = list()
		durations = list()
		for r in range(repeats):
			print("repeat", r + 1)
			start = time.time()
			score = self.evaluate(self.train_windows, self.train_labels, self.test_windows, self.test_labels)
			end = time.time()
			score = score * 100.0
			print('>#%d: %.3f' % (r + 1, score))
			print("Took", round((end - start) / 60, 2), "seconds")
			durations.append(end - start)
			scores.append(score)

		# summarize results
		self.summarize_results(scores)
		self.save_results(scores, durations)

	def update_parameters(self, parameters):
		self.parameters = parameters


	def classify(self):
		path = self.arguments.dir + "classify/"
		shared.verify_directory_exists(path)
		saved_model = self.load_model()
		results = []
		valid_timeseries = shared.get_collection_of_valid_timeseries(path)
		for timeseries_data in valid_timeseries:
			print("Classifying", timeseries_data.filename)
			# timeseries_data.labels = shared.extract_from_file(path + "C2sci124Xe-p3900004-labels.p")

			if self.arguments.cache:
				cache_filepath = self.arguments.dir + "cache/CNN-classify.p"
				if os.path.exists(cache_filepath):
					windows = shared.extract_from_file(cache_filepath)
					print("> Cached file loaded")
				else:
					print("> No cached file found at", cache_filepath, ". Creating")
					os.makedirs(os.path.dirname(cache_filepath), exist_ok=True)
					windows = self.segment_into_windows(timeseries_data, "classify")
					self.save_pickled_file(windows, cache_filepath)
			else:
				windows = self.segment_into_windows(timeseries_data, "classify")

			start = time.time()

			train_test_windows_filepath = self.arguments.dir + "CNN/train_test_windows.p"
			train_test_windows = shared.extract_from_file(train_test_windows_filepath)
			testX = train_test_windows[1][0]
			testy = train_test_windows[1][1]
			predictions = saved_model.predict(windows, batch_size=32, verbose=1)
			hmm, accuracy = saved_model.evaluate(windows, testy, batch_size=32, verbose=0)
			print("Accuracy", accuracy)
			peak_counts = np.argmax(predictions, axis=1)
			peak_count = sum(peak_counts)
			end = time.time()
			print("> Number of peaks in file", timeseries_data.filename + ":", peak_count)
			if self.arguments.verbose: print("> Processing took:", round(end - start, 4), "seconds")
			results.append((timeseries_data.filename, peak_count))
			if self.arguments.graphs:
				for i, window in enumerate(windows):
					if peak_counts[i] == 1:
						plt.plot(window[:, 0], window[:, 1])
						plt.title("Labelled as " + str(peak_counts[i]))
						plt.show()

	def load_model(self):
		model_filepath = self.arguments.dir + "CNN/model"
		shared.verify_directory_exists(model_filepath)
		loaded_model = tf.keras.models.load_model(model_filepath)
		return loaded_model


	# def set_training_and_test_data(self, train_filenames, test_filenames):
	# 	self.train_filenames = train_filenames
	# 	self.test_filenames = test_filenames


if __name__ == '__main__':
	CLI = shared.generate_arg_parser("The one-dimensional convolutional neural network (CNN) uses discriminative supervised learning to count particles")
	args = CLI.parse_args()

	params = CNN.default_parameters
	params["prune"] = True
	params["window-size"] = 50
	params["prune-factor"] = 1000

	model = CNN(args, params)

	# filename = "C2sci124Xe-p3900006.p"
	# timeseries_A = pickle.load(open(filename, "rb"))
	# timeseries_A = timeseries_A[:5000000]
	# labels = pickle.load(open(filename[:-2] + "-labels.p", "rb"))
	# timeseries_data = shared.TimeseriesData(timeseries_A, filename, labels)
	# model.segment_into_windows(timeseries_data, "train")
	if args.mode == "classify":
		model.classify()
	elif args.mode == "train":
		model.train()
