import argparse
import csv
import time
import pickle
import os
from scipy.interpolate import CubicSpline
import numpy as np
from dtaidistance import dtw
from dtaidistance import dtw_visualisation as dtwvis
from collections import defaultdict
import matplotlib.pyplot as plt
import shared
import matplotlib.patches as mpatches
import more_itertools as mit
from numpy import unravel_index


class Segment:
	def __init__(self, timeseries, label=None, id=None):
		self.weight = 1
		self.timeseries = timeseries
		self.label = label
		self.id = id

	def __repr__(self):
		if self.id is None:
			segment_name = "Seg"
		else:
			segment_name = "Seg #" + str(self.id)
		return segment_name + ": {weight=" + str(self.weight) + ", label=" + str(self.label) + "}"


class STMF:
	def __init__(self, arguments, working_directory="./"):
		self.arguments = arguments
		self.templates = None

		self.labelled_training_segments = None
		self.offset_to_narrow_down_peak = 200
		self.baseline_threshold = 0.01
		self.working_directory = working_directory
		self.results = {"correct": 0, "incorrect": 0, "nomatch": 0}
		self.output = [[None for _ in range(10)] for _ in range(28)]
		self.output[0] = ["Correct-incorrect-nomatch", "Scores", "Time took(s)", "Average DTW distance", "+/-",
						  "Train filenames", "Test filenames", "Parameter", "Parameter value"]

	def prepare_data(self):
		start = time.time()
		timeseries = pickle.load(open("Raw data/C2sci124Xe-p3900000.p", "rb"))
		peak_times_labelled = pickle.load(open("./Labels/C2sci124Xe-p3900000.p", "rb"))
		labelled_training_segments = defaultdict(list)
		peak_id = 1
		for peak_time in peak_times_labelled:
			peak_center_index = np.where(timeseries[:, 0] == peak_time)[0][0]
			peak_lb = peak_center_index - self.offset_to_narrow_down_peak
			peak_up = peak_center_index + self.offset_to_narrow_down_peak
			peak_timeseries = timeseries[peak_lb:peak_up]

			peak_timeseries = self.trim_to_peak_boundaries(peak_timeseries)
			print(str(peak_id) + "/" + str(len(peak_times_labelled)))
			if self.arguments.graphs:
				plt.plot(peak_timeseries[:, 0], peak_timeseries[:, 1])
				plt.show()
			peaks_in_segment = len(np.intersect1d(peak_times_labelled, peak_timeseries[:, 0]))
			if peaks_in_segment > 0:
				labelled_training_segments[str(peaks_in_segment)].append(
					Segment(peak_timeseries[:, 1], peaks_in_segment, peak_id))
			peak_id += 1
		end = time.time()
		with open(self.working_directory + "train.p", "wb") as file:
			pickle.dump(labelled_training_segments, file)
		print(end - start, " took to prepare 1 file timeseries")
		self.labelled_training_segments = labelled_training_segments

	def trim_to_peak_boundaries(self, peak_timeseries):
		timeseries_indices_above_baseline = np.where(peak_timeseries[:, 1] > self.baseline_threshold)
		grouped_indices_over_baseline = [list(group) for group in
										 mit.consecutive_groups(list(timeseries_indices_above_baseline)[0])]
		peak_indices = max(grouped_indices_over_baseline, key=len)
		peak_timeseries = peak_timeseries[peak_indices]
		index_of_left_boundary = peak_indices[0]
		index_of_right_boundary = peak_indices[-1]
		peak_timeseries = peak_timeseries[index_of_left_boundary:index_of_right_boundary]

		return peak_timeseries

	def plot_peak_boundary(self, peak_timeseries, left_boundary, right_boundary):
		fig = plt.figure()
		ax = fig.add_subplot(111)
		colors = np.where(peak_timeseries[:, 1] > self.baseline_threshold, "#FAAF00", "#00799C")
		plt.scatter(peak_timeseries[:, 0], peak_timeseries[:, 1], c=colors, linewidth=1)
		plt.xlabel("Time in ns", fontsize=15)
		plt.ylabel("Voltage in V", fontsize=15)
		legend_labels = [mpatches.Patch(color='#FAAF00', label='Voltages above baseline'),
						 mpatches.Patch(color='#00799C', label='Voltages below baseline'),
						 mpatches.Patch(color='#FAAF00', label='Detected bounds', alpha=0.1)]
		plt.legend(handles=legend_labels)
		plt.axvspan(peak_timeseries[:, 0][left_boundary], peak_timeseries[:, 0][right_boundary], color='#FAAF00',
					alpha=0.1)
		plt.show()

	def load_training_data(self):
		with open(self.working_directory + "train.p", "rb") as file:
			self.labelled_training_segments = pickle.load(file)
		del self.labelled_training_segments["0"]

	def train(self):
		if self.labelled_training_segments is None:
			print("Assemble or load training data first")
			return
		start = time.time()
		templates = self.SMTF_training()
		end = time.time()
		pickle.dump(templates, open(self.working_directory + 'templates.p', "wb"))
		print("Template found in", end - start)

	def SMTF_training(self):

		labels = self.labelled_training_segments.keys()

		templates = []
		for label in labels:
			segments_of_label = self.labelled_training_segments[label]
			template = self.averaging_scheme(segments_of_label)
			templates.append(template)
		return templates

	def averaging_scheme(self, segments_of_label):
		total_segments = len(segments_of_label)
		while len(segments_of_label) > 1:
			sequences = [seg.timeseries for seg in segments_of_label]
			ds = dtw.distance_matrix_fast(sequences[:100])
			a_index, b_index = unravel_index(ds.argmax(), ds.shape)
			a = segments_of_label[a_index]
			b = segments_of_label[b_index]

			c = self.mean_averaging(a, b)
			c.weight = a.weight + b.weight

			segments_of_label.remove(a)
			segments_of_label.remove(b)
			segments_of_label.append(c)
			print(str(100 - round((len(segments_of_label) / total_segments) * 100)) + "% done")

		return segments_of_label[0]

	def CDTW(self, a, b):
		sequence_a = a.timeseries
		sequence_b = b.timeseries
		warping_path = dtw.warping_path(sequence_a, sequence_b)

		timeseries_c = np.zeros((len(warping_path), 2))
		for k in range(len(warping_path)):
			i, j = warping_path[k]
			x = ((i * a.weight) + (j * b.weight)) / (a.weight + b.weight)
			y = ((sequence_a[i] * a.weight)
				 + (sequence_b[j] * b.weight)) \
				/ (a.weight + b.weight)
			timeseries_c[k] = (x, y)
		cubic_spline = CubicSpline(timeseries_c[:, 0], timeseries_c[:, 1])
		new_x = cubic_spline.x
		new_y = cubic_spline(new_x)
		timeseries_c_dash = np.stack((new_x, new_y), axis=1)
		if self.arguments.graphs:
			plot_sequence(sequence_a, "Sequence A", "#1f77b4")
			plot_sequence(sequence_b, "Sequence B", "#ff7f0e")
			plot_sequence(timeseries_c[:, 1], "Average of A and B", "#2ca02c")
			plot_sequence(timeseries_c_dash[:, 1], "Average of A and B (Resampled", "#026502")
			plt.show()
		return Segment(timeseries_c_dash[:, 1], a.label, -1)

	def DTW_averaging(self, a, b):
		sequence_a = a.timeseries
		sequence_b = b.timeseries
		warping_path = dtw.warping_path(sequence_a, sequence_b)
		averaged_sequence = np.zeros(len(warping_path))
		for k in range(len(warping_path)):
			i, j = warping_path[k]
			averaged_sequence_at_k = ((sequence_a[i] * a.weight)
									  + (sequence_b[j] * b.weight)) \
									 / (a.weight + b.weight)
			averaged_sequence[k] = averaged_sequence_at_k
		if self.arguments.graphs:
			plot_sequence(sequence_a, "Sequence A")
			plot_sequence(sequence_b, "Sequence B")
			plot_sequence(averaged_sequence, "Average of A and B")
			plt.show()
			dtwvis.plot_warping(sequence_a, sequence_b, warping_path)
		return Segment(averaged_sequence, a.label, a.id + 1)

	def mean_averaging(self, a, b):
		if a.timeseries.shape[0] > b.timeseries.shape[0]:
			a.timeseries = a.timeseries[:b.timeseries.shape[0]]
		else:
			b.timeseries = b.timeseries[:a.timeseries.shape[0]]
		averaged_sequence = np.mean([a.timeseries, b.timeseries], axis=0)
		if self.arguments.graphs:
			plot_sequence(a, "Sequence A")
			plot_sequence(b, "Sequence B")
			plot_sequence(averaged_sequence, "Average of A and B")
			plt.show(block=False)

		return Segment(averaged_sequence, a.label, -1)

	def inspect_templates(self, template_name="templates"):
		with open(self.working_directory + template_name + '.p', "rb") as file:
			templates = pickle.load(file)
		figure, axis = plt.subplots(len(templates))
		figure.suptitle(template_name)
		for i in range(len(templates)):
			axis[i].plot(templates[i].timeseries, color="#00799C")

			axis[i].set_title("Template of " + str(templates[i].label) + " peak(s)")
		plt.tight_layout()
		plt.show(block=False)

	def inspect_DTW(self, peak_label):
		for i in range(len(self.labelled_training_segments[peak_label]) - 1):
			peakA_voltage = self.labelled_training_segments[peak_label][i].timeseries
			peakB_voltage = self.labelled_training_segments[peak_label][i + 1].timeseries
			path = dtw.warping_path(peakA_voltage, peakB_voltage)
			figure, axis = plt.subplots(2)
			dtwvis.plot_warping(peakA_voltage, peakB_voltage, path, None, figure, axis)
			plt.ylabel("Voltage in V")
			plt.xlabel("Time in ns")
			plt.show()

	def evaluate_test_segment(self, test_segment):
		template_sequences = [seg.timeseries for seg in self.templates]
		template_sequences.insert(0, test_segment.timeseries)
		ds = dtw.distance_matrix_fast(template_sequences, block=((0, 1), (0, len(template_sequences))))
		result = ds[0][1:]
		template_index = np.argmin(result)
		smallest_distance = min(result)
		smallest_template = self.templates[template_index]

		if self.arguments.verbose: print("Smallest template with label", smallest_template.label, "with score of",
										 round(smallest_distance, 2))

		if self.arguments.graphs and smallest_template.label > 1:
			figure, axis = plt.subplots(len(self.templates))

			for i, template in enumerate(self.templates):
				axis[i].set_title("Template " + str(template.label))
				axis[i].plot(template.timeseries)
				axis[i].plot(test_segment.timeseries, label="Test segment")

			figure.suptitle(
				"Similarity score (DTW distance) " + str(round(smallest_distance, 4)) + " for template " + str(
					smallest_template.label))
			plt.show()
		if test_segment.label is not None:
			if smallest_distance >= 1:
				self.results["nomatch"] += 1
			elif smallest_template.label == test_segment.label:
				self.results["correct"] += 1
			else:
				self.results["incorrect"] += 1
			return smallest_distance
		else:
			if smallest_distance >= 1:
				return smallest_distance, 0
			else:
				return smallest_distance, smallest_template.label

	def test(self, timeseries, labels):
		with open(self.working_directory + "DTW templates.p", "rb") as file:
			self.templates = pickle.load(file)
		DTW_distances = []
		start = time.time()

		timeseries_indices_above_baseline = np.where(timeseries[:, 1] > 0.01)
		grouped_indices_above_baseline = [list(group) for group in
										  mit.consecutive_groups(list(timeseries_indices_above_baseline)[0])]
		for group in grouped_indices_above_baseline:
			if len(group) > 50:
				ROI = timeseries[group]
				peaks_in_segment = len(np.intersect1d(labels, ROI[:, 0]))
				test_segment = Segment(ROI[:, 1], peaks_in_segment)
				DTW_distances.append(self.evaluate_test_segment(test_segment))
		end = time.time()

		print("Successfully classified", self.results["correct"], "out of", sum(self.results.values()), ". + ",
			  self.results["nomatch"], "without a match")
		print(str(round(self.results["correct"] / sum(self.results.values()) * 100)) + "%")
		print("Classification took", round(end - start, 2), "seconds")
		self.save_results(DTW_distances, end - start)

	def save_results(self, DTW_distances, duration):
		m, s = np.mean(DTW_distances), np.std(DTW_distances)
		print("Average time per ROI", round(duration / len(DTW_distances), 4))
		self.output[1][0] = str(self.results["correct"]) + "-" + str(self.results["incorrect"]) + "-" + str(
			self.results["nomatch"])
		self.output[1][1] = round(self.results["correct"] / sum(self.results.values()) * 100, 4)
		self.output[1][2] = duration
		self.output[1][3] = ('%.3f' % m)
		self.output[1][4] = ('%.3f' % s)

		output_filepath = self.working_directory + "output.csv"
		with open(output_filepath, 'w') as f:
			write = csv.writer(f)
			write.writerows(self.output)

	def compare_timeseries(self):
		for template_name in ["Mean-based templates", "DTW templates", "CDTW templates"]:
			self.inspect_templates(template_name)

	def classify(self):
		templates_loaded_successfully = self.load_templates()
		if not templates_loaded_successfully:
			return
		path = self.working_directory + "classify/"
		results = []
		DTW_distances = []
		if not os.path.exists(path):
			print("Error: Create a directory called 'classify' under the working directory")
			return
		files_and_folders = os.listdir(path)
		if len(files_and_folders) == 0:
			print("Error: No files found in folder: " + path)
			return
		for item in files_and_folders:
			if os.path.isfile(path + item):
				timeseries = shared.extract_timeseries(path + item)
				if timeseries is not None:
					ROIs = self.extract_ROI_segments(timeseries[1815000:])
					if len(ROIs) != 0:
						print("Classifying", item, "with", len(ROIs), "regions of interest")
						start = time.time()
						peak_count = 0
						for ROI in ROIs:
							DTW_distance, peaks_in_segment = self.evaluate_test_segment(ROI)
							peak_count += peaks_in_segment
							DTW_distances.append(DTW_distance)
						results.append((item, peak_count))
						end = time.time()
						if self.arguments.verbose:
							print("> Number of peaks in file", item + ":", peak_count)
							print("> Processing took:", round(end - start, 4), "seconds")
					else:
						print("Warning: No regions of interest found in", item)
				else:
					print("Warning: File", item, "is not in a supported filetype (.p, .trc, .csv). Skipping")
		self.save_classification_results(results, DTW_distances)

	def save_classification_results(self, results, DTW_distances):
		output = [["Filename", "Peak count", "Average DTW distance", "+/-"]]
		mean, standard_deviation = np.mean(DTW_distances), np.std(DTW_distances)

		for result in results:
			output.append([result[0], result[1]])
		output[1].append(str(round(mean, 3)))
		output[1].append(str(round(standard_deviation, 3)))
		filename = "Classification results " + time.strftime("%Y-%m-%d %H:%M:%S")
		filepath = self.working_directory + "STMF/" + filename + ".csv"
		with open(filepath, 'w') as f:
			write = csv.writer(f)
			write.writerows(output)
		print("Successfully saved results in:", filepath)

	def extract_ROI_segments(self, timeseries):
		timeseries_indices_above_baseline = np.where(timeseries[:, 1] > 0.01)
		grouped_indices_above_baseline = [list(group) for group in
										  mit.consecutive_groups(list(timeseries_indices_above_baseline)[0])]
		ROIs = []
		for group in grouped_indices_above_baseline:
			if len(group) > 50:
				ROI = timeseries[group]
				ROI_segment = Segment(ROI[:, 1])
				ROIs.append(ROI_segment)
		return ROIs

	def load_templates(self):
		templates_filepath = self.working_directory + 'STMF/templates.p'
		if os.path.exists(templates_filepath):
			with open(templates_filepath, "rb") as file:
				templates = pickle.load(file)
				print("Number of templates loaded:", len(templates))
			self.templates = templates
			return True
		else:
			print("Template file was not found. Expected location:" + templates_filepath)
			return False


def plot_sequence(sequence, label, color=False, alpha=1.0, debug=True):
	# plt.title("Sequences")
	plt.xlabel("x")
	plt.ylabel("Voltage in mV")
	if not color:
		plt.plot(sequence, label=label, alpha=alpha)
	else:
		plt.plot(sequence, label=label, alpha=alpha, color=color)
	if debug:
		plt.legend(loc="center right")


def subtract_peak(original_timeseries_segment, peak_timeseries):
	mask = np.where(np.equal(original_timeseries_segment[:, 0], peak_timeseries))
	intersect, comm1, comm2 = np.intersect1d(original_timeseries_segment[:, 0], peak_timeseries, return_indices=True)
	cropped_timeseries = np.delete(original_timeseries_segment, comm1, 0)
	return cropped_timeseries


if __name__ == '__main__':
	CLI = argparse.ArgumentParser(
		description="The shape-based template matching framework (STMF) is used to train and classify timeseries data using an algorithm based on generated templates of shapes",
		prog="PPCA")
	modes = ["classify", "train"]
	CLI.add_argument('mode', type=str, default='classify', choices=modes, help="define the mode of the algorithm")
	CLI.add_argument('--verbose', '-v', action='store_true', help='print extra information useful for debugging')
	CLI.add_argument('--save', '-s', action='store_true', help='save results in a CSV file')
	CLI.add_argument('--flip-y', '-f', action='store_true',
					 help='reflect data across the x axis by multiply all y values by -1 (.trc files only)')

	CLI.add_argument('--graphs', '-g', action='store_true', help='interactively asks to show graphs at key points')
	CLI.add_argument('--x-unit', '-x', type=str, help='set unit for x axis of graphs', default="V")
	CLI.add_argument('--y-unit', '-y', type=str, help='set unit for y axis of graphs', default="seconds")
	args = CLI.parse_args()
	model = STMF(args)

	if args.mode == "classify":
		model.classify()
	elif args.mode == "train":
		model.train()
