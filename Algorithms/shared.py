import argparse
import os
import pickle
import sys
import matplotlib.pyplot as plt

import numpy as np
import datetime
import struct


class TimeseriesInput:
	def __init__(self, timeseries, filename=None, labels=None):
		self.timeseries = timeseries
		self.filename = filename
		self.labels = labels


def query_yes_no(question):
	yes = {'yes', 'y', 'ye'}
	no = {'no', 'n', ''}
	print(question, "[y/n]: ")
	while True:
		choice = input().lower()
		if choice in yes:
			return True
		elif choice in no:
			return False
		else:
			print("Invalid input, try again [y/n]:")

if __name__ == '__main__':
	response = query_yes_no("Would you like to see a graph?")
	print(response)

def extract_timeseries(filepath):
	extension = os.path.splitext(filepath)[1]
	timeseries = None
	if extension == ".trc":
		x, y, metadata = readTrc(filepath)
		timeseries = np.stack((x, y), axis=1)
	elif extension == ".p":
		with open(filepath, "rb") as file:
			timeseries = pickle.load(file)
	elif extension == ".csv":
		timeseries = np.genfromtxt(filepath + ".csv", delimiter=',')

	if timeseries is not None and len(timeseries.shape) == 2:

		return timeseries
	else:
		return None


def extract_from_file(filepath):
	extension = os.path.splitext(filepath)[1]
	if extension == ".p":
		with open(filepath, "rb") as file:
			return pickle.load(file)
	elif extension == ".csv":
		return np.genfromtxt(filepath + ".csv", delimiter=',')
	else:
		return None

def plot_timeseries(timeseries):
	plt.plot(timeseries[:, 0], timeseries[:, 1])
def get_collection_of_valid_timeseries(directory, with_labels=False, flip_y=False):
	valid_timeseries = []
	files_and_folders = os.listdir(directory)
	if len(files_and_folders) == 0:
		print("Error: No files found in folder: " + directory)
		sys.exit()
	for item in files_and_folders:
		if os.path.isfile(directory + item):
			timeseries = extract_timeseries(directory + item)
			if flip_y:
				timeseries[:, 1] = timeseries[:, 1] * -1

			if timeseries is not None:
				timeseries_data = TimeseriesInput(timeseries, item)
				if with_labels:
					labels_filename = os.path.splitext(timeseries_data.filename)[0] + "-labels.p"
					if os.path.isfile(directory + labels_filename):
						labels = np.array(extract_from_file(directory + labels_filename))
						if len(labels.shape) == 1:
							timeseries_data.labels = labels
						else:
							print("Warning: Labels file contains more than 1 dimension. Ignoring the labels file and the corresponding timeseries")
							continue
					else:
						print("Warning: Corresponding labels  file not found at:", labels_filename, ". Ignoring the labels file and the corresponding timeseries")
						continue

				valid_timeseries.append(timeseries_data)
			else:
				print("Warning: File", item, "is not in a supported filetype (.p, .trc, .csv) for a time series. Ignored")
	if len(valid_timeseries) == 0:
		print("Error: No valid time series found in folder: " + directory)
		sys.exit()
	else:
		return valid_timeseries

def verify_directory_exists(path, should_quit=True):
	if not os.path.exists(path):
		user_text = "Create the following directory at: " + str(path)
		if should_quit:
			print("Error:", user_text)
			sys.exit()
		else:
			print("Warning:", user_text)




""" 
Little helper function to load data from a .trc binary file.
This is the file format used by LeCroy oscilloscopes.
M. Betz 09/2015
"""


def readTrc(fName):
	"""
        Reads .trc binary files from LeCroy Oscilloscopes.
        Decoding is based on LECROY_2_3 template.
        [More info](http://forums.ni.com/attachments/ni/60/4652/2/LeCroyWaveformTemplate_2_3.pdf)

        Parameters
        -----------
        fName = filename of the .trc file

        Returns
        -----------
        x: array with sample times [s],

        y: array with sample  values [V],

        d: dictionary with metadata


        M. Betz 09/2015
    """
	with open(fName, "rb") as fid:
		data = fid.read(50).decode()
		wdOffset = data.find('WAVEDESC')

		# ------------------------
		# Get binary format / endianess
		# ------------------------
		if readX(fid, '?', wdOffset + 32):  # 16 or 8 bit sample format?
			smplFmt = "int16"
		else:
			smplFmt = "int8"
		if readX(fid, '?', wdOffset + 34):  # Big or little endian?
			endi = "<"
		else:
			endi = ">"

		# ------------------------
		# Get length of blocks and arrays:
		# ------------------------
		lWAVE_DESCRIPTOR = readX(fid, endi + "l", wdOffset + 36)
		lUSER_TEXT = readX(fid, endi + "l", wdOffset + 40)
		lTRIGTIME_ARRAY = readX(fid, endi + "l", wdOffset + 48)
		lRIS_TIME_ARRAY = readX(fid, endi + "l", wdOffset + 52)
		lWAVE_ARRAY_1 = readX(fid, endi + "l", wdOffset + 60)
		lWAVE_ARRAY_2 = readX(fid, endi + "l", wdOffset + 64)

		d = dict()  # Will store all the extracted Metadata

		# ------------------------
		# Get Instrument info
		# ------------------------
		d["INSTRUMENT_NAME"] = readX(fid, "16s", wdOffset + 76).decode().split('\x00')[0]
		d["INSTRUMENT_NUMBER"] = readX(fid, endi + "l", wdOffset + 92)
		d["TRACE_LABEL"] = readX(fid, "16s", wdOffset + 96).decode().split('\x00')[0]

		# ------------------------
		# Get Waveform info
		# ------------------------
		d["WAVE_ARRAY_COUNT"] = readX(fid, endi + "l", wdOffset + 116)
		d["PNTS_PER_SCREEN"] = readX(fid, endi + "l", wdOffset + 120)
		d["FIRST_VALID_PNT"] = readX(fid, endi + "l", wdOffset + 124)
		d["LAST_VALID_PNT"] = readX(fid, endi + "l", wdOffset + 128)
		d["FIRST_POINT"] = readX(fid, endi + "l", wdOffset + 132)
		d["SPARSING_FACTOR"] = readX(fid, endi + "l", wdOffset + 136)
		d["SEGMENT_INDEX"] = readX(fid, endi + "l", wdOffset + 140)
		d["SUBARRAY_COUNT"] = readX(fid, endi + "l", wdOffset + 144)
		d["SWEEPS_PER_ACQ"] = readX(fid, endi + "l", wdOffset + 148)
		d["POINTS_PER_PAIR"] = readX(fid, endi + "h", wdOffset + 152)
		d["PAIR_OFFSET"] = readX(fid, endi + "h", wdOffset + 154)
		d["VERTICAL_GAIN"] = readX(fid, endi + "f", wdOffset + 156)  # to get floating values from raw data :
		d["VERTICAL_OFFSET"] = readX(fid, endi + "f", wdOffset + 160)  # VERTICAL_GAIN * data - VERTICAL_OFFSET
		d["MAX_VALUE"] = readX(fid, endi + "f", wdOffset + 164)
		d["MIN_VALUE"] = readX(fid, endi + "f", wdOffset + 168)
		d["NOMINAL_BITS"] = readX(fid, endi + "h", wdOffset + 172)
		d["NOM_SUBARRAY_COUNT"] = readX(fid, endi + "h", wdOffset + 174)
		d["HORIZ_INTERVAL"] = readX(fid, endi + "f", wdOffset + 176)  # sampling interval for time domain waveforms
		d["HORIZ_OFFSET"] = readX(fid, endi + "d",
								  wdOffset + 180)  # trigger offset for the first sweep of the trigger, seconds between the trigger and the first data point
		d["PIXEL_OFFSET"] = readX(fid, endi + "d", wdOffset + 188)
		d["VERTUNIT"] = readX(fid, "48s", wdOffset + 196).decode().split('\x00')[0]
		d["HORUNIT"] = readX(fid, "48s", wdOffset + 244).decode().split('\x00')[0]
		d["HORIZ_UNCERTAINTY"] = readX(fid, endi + "f", wdOffset + 292)
		d["TRIGGER_TIME"] = getTimeStamp(fid, endi, wdOffset + 296)
		d["ACQ_DURATION"] = readX(fid, endi + "f", wdOffset + 312)
		d["RECORD_TYPE"] = \
			["single_sweep", "interleaved", "histogram", "graph", "filter_coefficient", "complex", "extrema",
			 "sequence_obsolete", "centered_RIS", "peak_detect"][readX(fid, endi + "H", wdOffset + 316)]
		d["PROCESSING_DONE"] = \
			["no_processing", "fir_filter", "interpolated", "sparsed", "autoscaled", "no_result", "rolling",
			 "cumulative"][
				readX(fid, endi + "H", wdOffset + 318)]
		d["RIS_SWEEPS"] = readX(fid, endi + "h", wdOffset + 322)
		d["TIMEBASE"] = \
			['1_ps/div', '2_ps/div', '5_ps/div', '10_ps/div', '20_ps/div', '50_ps/div', '100_ps/div', '200_ps/div',
			 '500_ps/div', '1_ns/div', '2_ns/div', '5_ns/div', '10_ns/div', '20_ns/div', '50_ns/div', '100_ns/div',
			 '200_ns/div', '500_ns/div', '1_us/div', '2_us/div', '5_us/div', '10_us/div', '20_us/div', '50_us/div',
			 '100_us/div', '200_us/div', '500_us/div', '1_ms/div', '2_ms/div', '5_ms/div', '10_ms/div', '20_ms/div',
			 '50_ms/div', '100_ms/div', '200_ms/div', '500_ms/div', '1_s/div', '2_s/div', '5_s/div', '10_s/div',
			 '20_s/div',
			 '50_s/div', '100_s/div', '200_s/div', '500_s/div', '1_ks/div', '2_ks/div', '5_ks/div', 'EXTERNAL'][
				readX(fid, endi + "H", wdOffset + 324)]
		d["VERT_COUPLING"] = ['DC_50_Ohms', 'ground', 'DC_1MOhm', 'ground', 'AC,_1MOhm'][
			readX(fid, endi + "H", wdOffset + 326)]
		d["PROBE_ATT"] = readX(fid, endi + "f", wdOffset + 328)
		d["FIXED_VERT_GAIN"] = \
			['1_uV/div', '2_uV/div', '5_uV/div', '10_uV/div', '20_uV/div', '50_uV/div', '100_uV/div', '200_uV/div',
			 '500_uV/div', '1_mV/div', '2_mV/div', '5_mV/div', '10_mV/div', '20_mV/div', '50_mV/div', '100_mV/div',
			 '200_mV/div', '500_mV/div', '1_V/div', '2_V/div', '5_V/div', '10_V/div', '20_V/div', '50_V/div',
			 '100_V/div',
			 '200_V/div', '500_V/div', '1_kV/div'][readX(fid, endi + "H", wdOffset + 332)]
		d["BANDWIDTH_LIMIT"] = ['off', 'on'][readX(fid, endi + "H", wdOffset + 334)]
		d["VERTICAL_VERNIER"] = readX(fid, endi + "f", wdOffset + 336)
		d["ACQ_VERT_OFFSET"] = readX(fid, endi + "f", wdOffset + 340)
		d["WAVE_SOURCE"] = readX(fid, endi + "H", wdOffset + 344)
		d["USER_TEXT"] = readX(fid, "{0}s".format(lUSER_TEXT), wdOffset + lWAVE_DESCRIPTOR).decode().split('\x00')[0]

		# ------------------------
		# Get main sample data with the help of numpys .fromfile(
		# ------------------------
		fid.seek(wdOffset + lWAVE_DESCRIPTOR + lUSER_TEXT + lTRIGTIME_ARRAY + lRIS_TIME_ARRAY)  # Seek to WAVE_ARRAY_1
		y = np.fromfile(fid, smplFmt, lWAVE_ARRAY_1)
		if endi == ">":
			y.byteswap(True)
		y = d["VERTICAL_GAIN"] * y - d["VERTICAL_OFFSET"]
		x = np.arange(1, len(y) + 1) * d["HORIZ_INTERVAL"] + d["HORIZ_OFFSET"]
	return x, y, d


def readX(fid, fmt, adr=None):
	""" extract a byte / word / float / double from the binary file """
	nBytes = struct.calcsize(fmt)
	if adr is not None:
		fid.seek(adr)
	s = struct.unpack(fmt, fid.read(nBytes))
	if (type(s) == tuple):
		return s[0]
	else:
		return s


def getTimeStamp(fid, endi, adr):
	""" extract a timestamp from the binary file """
	s = readX(fid, endi + "d", adr)
	m = readX(fid, endi + "b")
	h = readX(fid, endi + "b")
	D = readX(fid, endi + "b")
	M = readX(fid, endi + "b")
	Y = readX(fid, endi + "h")
	trigTs = datetime.datetime(Y, M, D, h, m, int(s), int((s - int(s)) * 1e6))
	return trigTs


def generate_arg_parser(description):
	filename_no_extension = os.path.splitext(os.path.basename(__file__))[0]
	CLI = argparse.ArgumentParser(
		description=description,
		prog=filename_no_extension)
	modes = ["classify", "train"]
	CLI.add_argument('mode', type=str, default='classify', choices=modes, help="define the mode of the algorithm")
	CLI.add_argument('--verbose', '-v', action='store_true', help='print extra information useful for debugging')
	CLI.add_argument('--dir', '-d', default="./", type=str, help='Path to working directory')
	CLI.add_argument('--save', '-s', action='store_true', help='save results in a CSV file')
	CLI.add_argument('--flip-y', '-f', action='store_true',
					 help='reflect data across the x axis by multiply all y values by -1 (.trc files only)')

	CLI.add_argument('--graphs', '-g', action='store_true', help='interactively asks to show graphs at key points')
	CLI.add_argument('--no-interact', '-n', action='store_true', help='turn non-interactive mode on')
	CLI.add_argument('--cache', '-c', action='store_true', help='cache segmentation step to improve speed performance at the cost of storage')
	CLI.add_argument('--x-unit', '-x', type=str, help='set unit for x axis of graphs', default="V")
	CLI.add_argument('--y-unit', '-y', type=str, help='set unit for y axis of graphs', default="seconds")
	working_dir = CLI.parse_args().dir
	if os.path.exists(working_dir):
		return CLI
	else:
		print("Working directory", working_dir, "not found.")
		sys.exit()
def check_directory_exists(path):
	if not os.path.exists(path):
		print("Error: Directory at path", path, "not found")
		sys.exit()

# def ask_yes_no():
# 	yes = {'yes', 'y', 'ye', ''}
# 	no = {'no', 'n'}
#
# 	choice = input().lower()
# 	if choice in yes:
# 		return True
# 	elif choice in no:
# 		return False
# 	else:
# 		print("Warning: Please respond with 'yes' or 'no'")
